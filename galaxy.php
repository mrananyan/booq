<?php
/**
 * @project Galaxy Framework
 * @author Sargis Ananyan <ananyan@dr.com>
 * @version '.$version.'
 */
define('App', __dir__);
define('Models', App . '/Models/');
define('Views', App . '/Views/');
define('Controllers', App . '/Controllers/');
define('Public', App . '/Public/');
$version = '1.0.1 alpha';
echo "\e[0;31;42mGalaxy Framework dev tool.\e[0m\n\r\n";
if ($argc == 1) {
    $message = "What you want to do?\r\n";
    $message .= "\t1. Create controller\r\n";
    $message .= "\t2. Create model\r\n";
    echo $message;
    echo "Choose one option: ";
    $handle = fopen("php://stdin", "r");
    $line = fgets($handle);
    $option = intval(trim($line));
    fclose($handle);
    if ($option === 1) {
        echo "\r\n Making controller \r\n";
        echo "Write your controller name: ";
        $handle = fopen("php://stdin", "r");
        $line = fgets($handle);
        $name = ucfirst(trim($line));
        fclose($handle);
        $Controller = '<?php
/**
 * @project Galaxy Framework
 * @author Sargis Ananyan <ananyan@dr.com>
 * @version '.$version.'
 */
namespace Controllers;

use Galaxy\Controllers;
use Symfony\Component\HttpFoundation\Request;

class ' . $name . 'Controller extends Controllers
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
    }
    
}';
        file_put_contents(Controllers . $name . 'Controller.php', $Controller);
    } else if ($option === 2) {
        echo "\r\n Making model \r\n";
        echo "Write your model name: ";
        $handle = fopen("php://stdin", "r");
        $line = fgets($handle);
        $name = ucfirst(trim($line));
        fclose($handle);
        $Controller = '<?php
/**
 * @project Galaxy Framework
 * @author Sargis Ananyan <ananyan@dr.com>
 * @version '.$version.'
 */
namespace Models;

use Galaxy\Database;
use Galaxy\Model;

class ' . $name . ' extends Model
{
    public $table = \'' . strtolower($name) . '\';

    public function default()
    {
        global $GLOBALS;
        
        $this->table($this->table);
    }
}';
        file_put_contents(Models . $name . '.php', $Controller);
    }
    echo "\e\n Model created - " . $name . '.php';
    echo "\n";
}