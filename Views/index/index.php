<div class="login-form fl">
    <p class="login-title ">{translate=index_0}</p>
    <form action="/auth/login" method="post" onsubmit="return login(this);">
        <label for="login_email">{translate=index_1}</label>
        <input class="login-input" type="text" placeholder="{translate=index_1}" name="email" id="login_email">
        <label for="login_password">{translate=index_2}</label>
        <input class="login-input" type="password" placeholder="{translate=index_2}" name="password" id="login_password">
        <input type="checkbox" name="remember" id="remember">
        <button type="submit" class="flat_button button_wide login-button">login</button>
    </form>
</div>
<div class="login-content fr">
    <p class="login-title ">{translate=index_3}</p>
</div>