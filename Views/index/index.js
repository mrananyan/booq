new Checkbox(ge('remember'), {
    width: 200,
    checked: 0,
    label: '{translate=index_4}',
    onChange: function (v) {
        if (v){
            showMessage('{translate=index_5}', MESSAGE_INFO, {left: true});
        }else{
            showMessage('{translate=index_6}', MESSAGE_INFO, {left: true});
        }
    }
});