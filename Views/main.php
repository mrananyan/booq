<!doctype html>
<html lang="{language_iso}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{title}</title>
    <meta name="Description" content="Smart and minimalistic CRM">
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/site.css">
    <script src="/js/core.js" type="application/javascript"></script>
    <script src="/js/events.js" type="application/javascript"></script>
    <script src="/js/dom.js" type="application/javascript"></script>
    <script src="/js/ajax.js" type="application/javascript"></script>
    <script src="/js/langs.js" type="application/javascript"></script>
    <script src="/js/cookie.js" type="application/javascript"></script>
    <script src="/js/cc.js" type="application/javascript"></script>
    {st_files}
</head>
<body>
{header}
<div id="content">
    <div class="wrap">
        <div class="page_block" id="page_block">
            {content}
        </div>
        {footer}
    </div>
</div>
<script>
    const config = {
        uid: 1,
        hid: 1,
        domain: 1,
        tab_id: '{tab_id}',
    };
    onDomReady(function () {
        {js}
    });
</script>
</body>
</html>