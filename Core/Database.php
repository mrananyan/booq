<?php
/**
 * @project Galaxy Framework
 * @author Sargis Ananyan <ananyan@dr.com>
 * @version 1.0.1 alpha
 */

namespace Galaxy;


class Database
{
    protected $inited = [];
    protected $authed = false;
    protected $connection = [];
    protected $result = [];
    protected $server = 'default';
    protected $tableName = '';
    protected $whereString = '';
    protected $queryOrder = '';
    protected $column = '';
    protected $values = '';
    protected $pageReqs = [];
    protected $httpQuery = [];
    protected $join = '';
    protected $onStr = '';
    protected $groupBy = '';

    /**
     * Database constructor.
     * @param bool $server
     */
    public function __construct($server = false)
    {
        $this->authed = (isset($_SESSION["token"])) ? true : false;
        $this->server = $server;
    }

    /**
     * @param bool $server
     * @return mixed
     */
    private function init($server = false)
    {
        global $mysql_servers;
        if (!$server) {
            $server = 'default';
        }
        if (!isset($this->inited[$server])) {
            $config = $mysql_servers[$server];
            $this->inited[$server] = mysqli_connect($config['DB_HOST'], $config['DB_USERNAME'], $config['DB_PASSWORD'], $config['DB_BASE']);
            if (mysqli_connect_errno()) {
                http_response_code(500);
                exit;
                // die('Connecting to MySQL failed: ' . mysqli_connect_error());
            } else {
                $this->query("SET NAMES 'utf8mb4'", $server);
                $this->query("SET CHARACTER SET 'utf8mb4'", $server);
                $this->query("SET SESSION collation_connection = 'utf8mb4_unicode_ci'", $server);

            }
        }
        return $this->inited[$server];
    }

    /**
     * @param $string
     * @param bool $server
     * @return string
     */
    public function escape($string, $server = false)
    {
        if (empty($this->connection)) {
            $this->connection = $this->init($server);
        }
        return $this->connection ? mysqli_real_escape_string($this->connection, $string) : addslashes($string);
    }


    /**
     * @param string $query
     * @param bool $server
     * @param bool $fetch
     * @return array|bool|\mysqli_result
     */
    public function query($query = '', $server = false, $fetch = false)
    {
        global $config, $mysql_servers;

        if (empty($this->connection) OR !isset($this->inited[$server])) {
            $this->connection = $this->init($server);
        }
        $this->result = mysqli_query($this->connection, $query);
        if (!$this->result && $config['debug']) {
            http_response_code(500);
            $dd = debug_backtrace();
            $files = '<h3>Error in file</h3><ol>';
            $index = 0;
            $code_lines = '<pre><code class="php">';
            foreach ($dd as $file) {
                if ($index == 2) {
                    $source = file($file['file']);
                    $code_lines .= $source[$file['line'] - 1];
                    /*for ($i = $file['line'] - 4; $i <= $file['line'] + 10; $i++){
                        $code_lines .= $source[$i].'<br>';
                    }*/
                    $code_lines .= '</code></pre>';
                    $files .= '<li style="color: blue" >File : ' . $file['file'] . ' as <b style="color: #ff0047">' . $file['line'] . '</b> line</li>';
                } else {
                    $files .= '<li>File : ' . $file['file'] . ' as <b style="color: #ff0047">' . $file['line'] . '</b> line</li>';
                }
                $index++;
            }

            $files .= '</ol><hr><h3>Part of code</h3>' . $code_lines . '<hr>';
            $internet = '<ul>';
            $internet = '<H1>Config</H1 - >' . $mysql_servers[$server]['DB_BASE'];
            $internet = '<ul>';
            $internet .= '<li><a href="https://www.google.am/search?q=MySql+error:+' . mysqli_errno($this->connection) . '&oq=mysql+error+' . mysqli_errno($this->connection) . '&aqs=chrome..69i57.19487j0j7&sourceid=chrome&ie=UTF-8" target="_blank">Search in Google</a></li>';
            $internet .= '<li><a href="https://duckduckgo.com/?q=Mysql+error+' . mysqli_errno($this->connection) . '&t=h_&ia=web" target="_blank">Search in DuckDuckGO</a></li>';
            $internet .= '<li><a href="https://stackoverflow.com/search?q=Mysql+error+' . mysqli_errno($this->connection) . '" target="_blank">Search in Stackoverf;ow</a></li>';
            $internet .= '</ul>';
            $stat = "<link rel=\"stylesheet\"
      href=\"//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.8/styles/default.min.css\">
<script src=\"//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.8/highlight.min.js\"></script><script>hljs.initHighlightingOnLoad();</script>";
            die($stat . "<h1 style=\"color:blue\">Quasar</h1><hr></br>Hey developer my name is Quasar, I can't do this query =>  <pre><code class='sql'>{$query}</code></pre>. Please fix the query by debug log and try again *_*<br><hr>" . mysqli_error($this->connection) . '<hr>' . $files . '<h3>If you can\'t solve this issue</h3>' . $internet);
        } elseif (!$this->result && !$this->authed) {
            die("Techical works");
        }
        /*if (!$fetch){
            $this->close();
        }*/
        return $this->result;
    }

    /**
     * @param $query
     * @param bool $server
     * @return array|object|null
     */
    public function multiQuery($query, $server = false)
    {
        $this->connection = $this->init($server);
        if (mysqli_multi_query($this->connection, $query)) {
            do {
                if ($result = mysqli_store_result($this->connection)) {
                    while ($this->result = mysqli_fetch_object($result)) {
                        if (!$this->result && $config['debug']) {
                            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Hey developer my name is Quasar, I can't do this query =>  <b>{$query}</b>. Please fix the query by debug log and try again *_*<br><hr>" . mysqli_error($this->connection));
                        } elseif (!$this->result && !$this->authed) {
                            die("Techical works");
                        }
                        return $this->result;
                    }
                    mysqli_free_result($result);
                }
            } while (mysqli_next_result($this->connection));
        }
    }

    /**
     * @param $query
     * @param bool $multiple
     * @param bool $server
     * @param bool $close
     * @return object|\stdClass|null
     */
    public function fetch($query, $multiple = false, $server = false, $close = true)
    {
        $this->result = $this->query($query, $server, true);
        if ($multiple) {
            $rows = new \stdClass();
            $i = 0;
            while ($row = mysqli_fetch_object($this->result)) {
                $rows->$i = $row;
                $i++;
            }
        } else {
            $rows = mysqli_fetch_object($this->result);
        }
        mysqli_free_result($this->result);
        if ($close) {
            $this->close();
        }
        return $rows;
    }

    /**
     * @param bool $table
     * @return $this
     */
    public function table($table = false)
    {
        if ($table) {
            if (isset(explode('.', $table)[1])) {
                $this->tableName = " {$table} ";
            } else {
                $this->tableName = " `{$table}` ";
            }
        } else {
            if ($config['debug']) {
                die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Please write valid table name");
            } else {
                die("Techical works");
            }
        }
        return $this;
    }

    /**
     * @param string $column
     * @return $this
     */
    public function select($column = '')
    {
        if (is_array($column)) {
            foreach ($column as $item) {
                if ($this->column) {
                    if (isset(explode('.', $item)[1])) {
                        $this->column .= ', ' . $item . ' ';
                    } else {
                        $this->column .= ', `' . $item . '` ';
                    }
                } else {
                    if (isset(explode('.', $item)[1])) {
                        $this->column = " {$item} ";
                    } else {
                        $this->column = " `{$item}` ";
                    }
                }
            }
        } elseif (isset($column)) {
            if (isset(explode('.', $column)[1])) {
                $this->column = " {$column} ";
            } else {
                $this->column = " `{$column}` ";
            }
        } else {
            if ($config['debug']) {
                die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Column must be array or string");
            } else {
                die("Techical works");
            }
        }
        return $this;
    }

    /**
     * @param string $column
     * @param string $type
     * @return $this
     */
    public function order($column = 'id', $type = 'DESC')
    {
        $this->queryOrder = " ORDER BY {$column} {$type}";
        return $this;
    }

    /**
     * @param string $column
     * @return $this
     */
    public function group($column = 'id')
    {
        $this->groupBy = " GROUP BY {$column} ";
        return $this;
    }

    /**
     * @param mixed $condition
     * @return $this
     */
    public function between($condition = false)
    {
        if ($condition AND is_array($condition)) {
            if (isset($condition[0]) AND is_array($condition[0])) {
                foreach ($condition as $con) {
                    if (empty($this->whereString)) {
                        if (isset(explode('.', $con[0])[1])) {
                            $this->whereString = " WHERE {$con[0]} BETWEEN {$con[1]} AND {$con[2]} ";
                        } else {
                            $this->whereString = " WHERE `{$con[0]}` BETWEEN {$con[1]} AND {$con[2]} ";
                        }
                    } else {
                        if (isset(explode('.', $con[0])[1])) {
                            $this->whereString .= " AND {$con[0]} BETWEEN {$con[1]} AND {$con[2]} ";
                        } else {
                            $this->whereString .= " AND `{$con[0]}` BETWEEN {$con[1]} AND {$con[2]} ";
                        }
                    }
                }
            }
        } elseif ($condition AND is_array($condition) AND isset($condition[0], $condition[1], $condition[2])) {
            if (empty($this->whereString)) {
                if (isset(explode('.', $con[0])[1])) {
                    $this->whereString = " WHERE {$condition[0]} BETWEEN {$condition[1]} AND {$condition[2]} ";
                } else {
                    $this->whereString = " WHERE `{$condition[0]}` BETWEEN {$condition[1]} AND {$condition[2]} ";
                }
            } else {
                if (isset(explode('.', $condition[0])[1])) {
                    $this->whereString .= " AND {$condition[0]} BETWEEN {$condition[1]} AND {$condition[2]} ";
                } else {
                    $this->whereString .= " AND `{$condition[0]}` BETWEEN {$condition[1]} AND {$condition[2]} ";
                }
            }
        } else {
            http_response_code(500);
            die('Condition alvays will be array!');
        }
        return $this;
    }

    /**
     * @param mixed $condition
     * @return $this
     */

    public function orBetween($condition = false)
    {
        if ($condition AND is_array($condition)) {
            if (isset($condition[0]) AND is_array($condition[0])) {
                foreach ($condition as $con) {
                    if (empty($this->whereString)) {
                        if (isset(explode('.', $con[0])[1])) {
                            $this->whereString = " WHERE {$con[0]} BETWEEN {$con[1]} AND {$con[2]} ";
                        } else {
                            $this->whereString = " WHERE `{$con[0]}` BETWEEN {$con[1]} AND {$con[2]} ";
                        }
                    } else {
                        if (isset(explode('.', $con[0])[1])) {
                            $this->whereString .= " OR {$con[0]} BETWEEN {$con[1]} AND {$con[2]} ";
                        } else {
                            $this->whereString .= " OR `{$con[0]}` BETWEEN {$con[1]} AND {$con[2]} ";
                        }
                    }
                }
            }
        } elseif ($condition AND is_array($condition) AND isset($condition[0], $condition[1], $condition[2])) {
            if (empty($this->whereString)) {
                if (isset(explode('.', $con[0])[1])) {
                    $this->whereString = " WHERE {$condition[0]} BETWEEN {$condition[1]} AND {$condition[2]} ";
                } else {
                    $this->whereString = " WHERE `{$condition[0]}` BETWEEN {$condition[1]} AND {$condition[2]} ";
                }
            } else {
                if (isset(explode('.', $condition[0])[1])) {
                    $this->whereString .= " OR {$condition[0]} BETWEEN {$condition[1]} AND {$condition[2]} ";
                } else {
                    $this->whereString .= " OR `{$condition[0]}` BETWEEN {$condition[1]} AND {$condition[2]} ";
                }
            }
        } else {
            http_response_code(500);
            die('Condition alvays will be array!');
        }

        return $this;
    }

    /**
     * @param array $where
     * @return $this
     */
    public function where($where = [])
    {
        if (is_array($where)) {
            foreach ($where as $item) {
                if ($this->whereString) {
                    if ($item[1] == 'in') {
                        if (isset(explode('.', $item[0])[1])) {
                            $this->whereString .= ' AND ' . $item[0] . ' IN(' . $this->escape($item[2]) . ')';
                        } else {
                            $this->whereString .= ' AND `' . $item[0] . '` IN(' . $this->escape($item[2]) . ')';
                        }
                    } else {
                        if (isset(explode('.', $item[0])[1])) {
                            $this->whereString .= ' AND ' . $item[0] . ' ' . $item[1] . ' \'' . $this->escape($item[2]) . '\'';
                        } else {
                            $this->whereString .= ' AND `' . $item[0] . '` ' . $item[1] . ' \'' . $this->escape($item[2]) . '\'';
                        }
                    }
                } else {
                    if ($item[1] == 'in') {
                        if (isset(explode('.', $item[0])[1])) {
                            $this->whereString = ' WHERE ' . $item[0] . ' IN(' . $this->escape($item[2]) . ')';
                        } else {
                            $this->whereString = ' WHERE `' . $item[0] . '` IN(' . $this->escape($item[2]) . ')';
                        }
                    } else {
                        if (isset(explode('.', $item[0])[1])) {
                            $this->whereString = ' WHERE ' . $item[0] . ' ' . $item[1] . ' \'' . $this->escape($item[2]) . '\'';
                        } else {
                            $this->whereString = ' WHERE `' . $item[0] . '` ' . $item[1] . ' \'' . $this->escape($item[2]) . '\'';
                        }
                    }
                }
            }
        } else {
            if ($config['debug']) {
                die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Please write valid where parameters");
            } else {
                die("Techical works");
            }
        }
        return $this;
    }

    /**
     * @param array $where
     * @return $this
     */
    public function orWhere($where = [])
    {
        if (is_array($where)) {
            foreach ($where as $item) {
                if ($this->whereString) {
                    if ($item[1] == 'in') {
                        if (isset(explode('.', $item[0])[1])) {
                            $this->whereString .= ' OR ' . $item[0] . ' IN(' . $this->escape($item[2]) . ')';
                        } else {
                            $this->whereString .= ' OR `' . $item[0] . '` IN(' . $this->escape($item[2]) . ')';
                        }
                    } else {
                        if (isset(explode('.', $item[0])[1])) {
                            $this->whereString .= ' OR ' . $item[0] . ' ' . $item[1] . ' \'' . $this->escape($item[2]) . '\'';
                        } else {
                            $this->whereString .= ' OR `' . $item[0] . '` ' . $item[1] . ' \'' . $this->escape($item[2]) . '\'';
                        }
                    }
                } else {
                    if ($item[1] == 'in') {
                        if (isset(explode('.', $item[0])[1])) {
                            $this->whereString = ' WHERE (' . $item[0] . ' IN(' . $this->escape($item[2]) . ')';
                        } else {
                            $this->whereString = ' WHERE (`' . $item[0] . '` IN(' . $this->escape($item[2]) . ')';
                        }
                    } else {
                        if (isset(explode('.', $item[0])[1])) {
                            $this->whereString = ' WHERE (' . $item[0] . ' ' . $item[1] . ' \'' . $this->escape($item[2]) . '\'';
                        } else {
                            $this->whereString = ' WHERE (`' . $item[0] . '` ' . $item[1] . ' \'' . $this->escape($item[2]) . '\'';
                        }
                    }
                }
            }

            if (substr($this->whereString, 1, 7) == "WHERE (") {
                $this->whereString .= ' ) ';
            }
        } else {
            if ($config['debug']) {
                die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Please write valid where parameters");
            } else {
                die("Techical works");
            }
        }
        return $this;
    }

    /**
     * @param array $where
     * @return $this
     */
    public function like($where = [])
    {
        if (is_array($where)) {
            foreach ($where as $k => $v) {
                if ($this->whereString) {
                    if (isset(explode('.', $k)[1])) {
                        $this->whereString .= " AND {$k} LIKE '%{$v}%' ";
                    } else {
                        $this->whereString .= " AND `{$k}` LIKE '%{$v}%' ";
                    }
                } else {
                    if (isset(explode('.', $k)[1])) {
                        $this->whereString = " WHERE {$k} LIKE %{$v}% ";
                    } else {
                        $this->whereString = " WHERE `{$k}` LIKE %{$v}% ";
                    }
                }
            }
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Please write valid like parameters");
        }
        return $this;
    }

    /**
     * @param array $where
     * @return $this
     */
    public function orLike($where = [])
    {
        if (is_array($where)) {
            foreach ($where as $k => $v) {
                if ($this->whereString) {
                    if (isset(explode('.', $k)[1])) {
                        $this->whereString .= " OR {$k} LIKE '%{$v}%' ";
                    } else {
                        $this->whereString .= " OR `{$k}` LIKE '%{$v}%' ";
                    }
                } else {
                    if (isset(explode('.', $k)[1])) {
                        $this->whereString = " WHERE {$k} LIKE %{$v}% ";
                    } else {
                        $this->whereString = " WHERE `{$k}` LIKE %{$v}% ";
                    }
                }
            }
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Please write valid like parameters");
        }
        return $this;
    }

    /**
     * @param array $where
     * @return $this
     */
    public function likeConcat($where = [])
    {
        if (is_array($where)) {
            if (strlen($this->whereString) > 3) {
                $tempAW = " AND ";
            } else {
                $tempAW = " WHERE ";
            }
            $temp = " CONCAT(";
            foreach ($where[0] as $col) {
                if ($temp == " CONCAT(") {
                    if (isset(explode('.', $col)[1])) {
                        $temp .= " {$col}, ' ' ";
                    } else {
                        $temp .= " `{$col}`, ' ' ";
                    }
                } else {
                    if (isset(explode('.', $col)[1])) {
                        $temp .= " , {$col}, ' ' ";
                    } else {
                        $temp .= " , `{$col}`, ' ' ";
                    }
                }
            }
            $temp .= ") ";
            $search = "";
            if (is_array($where[1])) {
                $search .= " ( ";
                foreach ($where[1] as $str) {
                    if ($search != " ( ") {
                        $search .= " OR " . $temp . " LIKE '%%{$str}%%'  ";
                    } else {
                        $search .= $temp . " LIKE '%%{$str}%%'  ";
                    }
                }
                $search .= " ) ";
            } else {
                $search = $temp . " LIKE %%{$where[1]}%%  ";
            }
            $this->whereString .= $tempAW . $search;
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Please write valid like parameters");
        }
        return $this;
    }

    /**
     * @param array $where
     * @return $this
     */
    public function orLikeConcat($where = [])
    {
        if (is_array($where)) {
            if (strlen($this->whereString) > 3) {
                $tempAW = " OR ";
            } else {
                $tempAW = " WHERE ";
            }
            $temp = " CONCAT(";
            foreach ($where[0] as $col) {
                if ($temp == " CONCAT(") {
                    if (isset(explode('.', $col)[1])) {
                        $temp .= " {$col}, ' ' ";
                    } else {
                        $temp .= " `{$col}`, ' ' ";
                    }
                } else {
                    if (isset(explode('.', $col)[1])) {
                        $temp .= " , {$col}, ' ' ";
                    } else {
                        $temp .= " , `{$col}`, ' ' ";
                    }
                }
            }
            $temp .= ") ";
            $search = "";
            if (is_array($where[1])) {
                $search .= " ( ";
                foreach ($where[1] as $str) {
                    if ($search != " ( ") {
                        $search .= " OR " . $temp . " LIKE '%%{$str}%%'  ";
                    } else {
                        $search .= $temp . " LIKE '%%{$str}%%'  ";
                    }
                }
                $search .= " ) ";
            } else {
                $search = $temp . " LIKE %%{$where[1]}%%  ";
            }
            $this->whereString .= $tempAW . $search;
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Please write valid like parameters");
        }
        return $this;
    }

    /**
     * @param bool $table
     * @return $this
     */
    public function leftJoin($table = false)
    {
        if ($this->tableName) {
            if ($table) {
                if (empty($this->join)) {
                    $this->join = " LEFT JOIN `{$table}`";
                } else {
                    $this->join .= " LEFT JOIN `{$table}`";
                }
            } else {
                die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Table name can`t be empty! ");
            }
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Hey developer you will call <b>table</b> method in first!");
        }
        return $this;
    }

    /**
     * @param bool $table
     * @return $this
     */
    public function rightJoin($table = false)
    {
        if ($this->tableName) {
            if ($table) {
                if (empty($this->join)) {
                    $this->join = " RIGHT JOIN `{$table}`";
                } else {
                    $this->join .= " RIGHT JOIN `{$table}`";
                }
            } else {
                die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Table name can`t be empty! ");
            }
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Hey developer you will call <b>table</b> method in first!");
        }
        return $this;
    }

    /**
     * @param bool $table
     * @return $this
     */
    public function innerJoin($table = false)
    {
        if ($this->tableName) {
            if ($table) {
                if (empty($this->join)) {
                    $this->join = " INNER JOIN `{$table}`";
                } else {
                    $this->join .= " INNER JOIN `{$table}`";
                }
            } else {
                die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Table name can`t be empty! ");
            }
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Hey developer you will call <b>table</b> method in first!");
        }
        return $this;
    }

    /**
     * @param array $on
     * @return $this
     */
    public function on($on = [])
    {
        if ($this->tableName) {
            if (is_array($on)) {
                if (is_array($on[1])) {
                    $onStr = '';
                    foreach ($on as $onr) {
                        if (is_array($onr[0])) {
                            foreach ($onr as $milti) {
                                if ($onStr) {
                                    $onStr .= " AND {$milti[0]} {$milti[1]} {$milti[2]} ";
                                } else {
                                    $onStr .= " ON {$milti[0]} {$milti[1]} {$milti[2]} ";
                                }
                            }
                        } else {
                            if ($onStr) {
                                $onStr .= " AND {$onr[0]} {$onr[1]} {$onr[2]} ";
                            } else {
                                $onStr .= " ON {$onr[0]} {$onr[1]} {$onr[2]} ";
                            }
                        }
                    }
                    $this->join .= $onStr;
                } else {
                    $this->join .= " ON {$on[0]} {$on[1]} {$on[2]} ";
                }
            }
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Hey developer you will call <b>table</b> method in first!");
        }
        return $this;
    }

    /**
     * @param array $on
     * @return $this
     */
    public function orOn($on = [])
    {
        if ($this->tableName) {
            if (is_array($on)) {
                if (is_array($on[1])) {
                    $onStr = '';
                    foreach ($on as $onr) {
                        if ($onStr) {
                            $onStr .= " OR {$onr[0]} {$onr[1]} {$onr[2]} ";
                        } else {
                            $onStr .= " ON {$onr[0]} {$onr[1]} {$onr[2]} ";
                        }
                    }
                    $this->join .= $onStr;
                } else {
                    $this->join .= " ON {$on[0]} {$on[1]} {$on[2]} ";
                }
            }
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Hey developer you will call <b>table</b> method in first!");
        }
        return $this;
    }

    /**
     * @param array $values
     * @param bool $server
     * @return array|bool|\mysqli_result
     */
    public function update($values = [], $server = false)
    {
        if (is_array($values)) {
            foreach ($values as $key => $value) {
                if (isset(explode('.', $key)[1])) {
                    if (strlen($this->column)) {
                        if (substr($value, 0, 1) !== '+' && isset(explode('+', $value)[1])) {
                            $this->column .= ', ' . $key . ' = ' . $value . '';
                        } else {
                            $this->column .= ', ' . $key . ' = \'' . $value . '\'';
                        }
                    } else {
                        if (substr($value, 0, 1) !== '+' && isset(explode('+', $value)[1])) {
                            $this->column = ' ' . $key . ' = ' . $value . '';
                        } else {
                            $this->column = ' ' . $key . ' = \'' . $value . '\'';
                        }
                    }
                } else {
                    if (strlen($this->column)) {
                        if (substr($value, 0, 1) !== '+' && isset(explode('+', $value)[1])) {
                            $this->column .= ', `' . $key . '` = ' . $value . '';
                        } else {
                            $this->column .= ', `' . $key . '` = \'' . $value . '\'';
                        }
                    } else {
                        if (substr($value, 0, 1) !== '+' && isset(explode('+', $value)[1])) {
                            $this->column = ' `' . $key . '` = ' . $value . '';
                        } else {
                            $this->column = ' `' . $key . '` = \'' . $value . '\'';
                        }
                    }
                }
            }
            $this->column = (strlen($this->column) > 1) ? $this->column : ' * ';
            $this->queryString = 'UPDATE ' . $this->tableName . ' ' . $this->join . ' ' . ' SET ' . $this->column . ' ' . $this->whereString;
            $this->result = $this->query($this->queryString, $server);
            $result = $this->result;
            $this->close();
            return $result;
        } else {
            return false;
        }
    }

    /**
     * @param array $values
     * @param bool $server
     * @return array|bool|\mysqli_result
     */
    public function insert($values = [], $server = false)
    {
        if (is_array($values)) {

            foreach ($values as $key => $value) {
                if (strlen($this->column)) {
                    $this->column .= ', `' . $key . '`';
                    $this->values .= ", '" . $value . "'";
                } else {
                    $this->column = '`' . $key . '`';
                    $this->values = "'" . $value . "'";
                }
            }
            $this->column = (strlen($this->column) > 1) ? $this->column : ' * ';
            $this->queryString = 'INSERT INTO ' . $this->tableName . ' (' . $this->column . ') VALUES (' . $this->values . ')';
            $this->result = $this->query($this->queryString, $server);
            $result = mysqli_insert_id($this->connection);
            $this->close();
            return $result;
        } else {
            return false;
        }
    }

    /**
     * @param bool $server
     * @return array|bool|\mysqli_result
     */
    public function delete($server = false)
    {
        if (isset($this->tableName)) {
            $this->queryString = 'DELETE FROM  ' . $this->tableName . '  ' . $this->join . ' ' . $this->whereString;
            $this->result = $this->query($this->queryString, $server);
            $result = $this->result;
            $this->close();
            return $result;
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Hey developer you will call <b>table</b> method in first!");
        }
    }

    /**
     * @param bool $server
     * @return array|bool|object|\stdClass|null
     */
    public function sum($column = 'id', $server = false)
    {
        if ($this->tableName) {
            $this->column = (strlen($this->column) > 1) ? $this->column : ' * ';
            $this->queryString = 'SELECT SUM(' . str_replace(['`', ' '], ['', ''], $this->tableName) . '.' . $column . ') as total FROM ' . $this->tableName . '  ' . $this->join . ' ' . $this->whereString . ';';
            $this->result = $this->fetch($this->queryString, false, $server, false);
            $result = $this->result->total;
            $this->close();
            return $result;
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Hey developer you will call <b>table</b> method in first!");
        }
    }

    /**
     * @param bool $server
     * @return array|bool|object|\stdClass|null
     */
    public function get($server = false)
    {
        if ($this->tableName) {
            $this->column = (strlen($this->column) > 1) ? $this->column : ' * ';
            $this->queryString = 'SELECT ' . $this->column . ' FROM ' . $this->tableName . '  ' . $this->join . ' ' . $this->whereString . ' ' . $this->queryOrder . ' ' . $this->groupBy . ';';
            $this->result = $this->fetch($this->queryString, false, $server, false);
            $result = $this->result;
            $this->close();
            return $result;
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Hey developer you will call <b>table</b> method in first!");
        }
    }

    /**
     * @param bool $server
     * @return array|bool|object|\stdClass|null
     */
    public function getAll($server = false)
    {
        if ($this->tableName) {
            $this->column = (strlen($this->column) > 1) ? $this->column : ' ' . str_replace(["`", " "], ["", ""], $this->tableName) . '.* ';
            $this->queryString = 'SELECT ' . $this->column . ' FROM ' . $this->tableName . ' ' . $this->join . ' ' . $this->whereString . ' ' . $this->groupBy . ' ' . $this->queryOrder . ';';
            $this->result = $this->fetch($this->queryString, true, $server, false);
            $result = $this->result;
            $this->close();
            return $result;
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Hey developer you will call <b>table</b> method in first!");
        }
    }

    /**
     * @param string $column
     * @param bool $server
     * @return int
     */
    public function count($column = '*', $server = false)
    {
        if ($this->tableName) {
            $count = $this->fetch("SELECT COUNT({$column}) as cnt FROM {$this->tableName} {$this->join} {$this->whereString} ", false, $server, false);
            $this->close();
            return $count->cnt;
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Hey developer you will call <b>table</b> method in first!");
        }
    }


    /**
     * @param int $limit
     * @param string $key
     * @param bool $server
     * @return \stdClass
     */
    public function getPages($limit = 10, $key = 'pagination', $server = false)
    {
        if ($this->tableName) {
            $url = strtok(getRequest('REQUEST_URI'), '?');
            $this->pageReqs = array_merge($_GET, $_POST);
            $table = '*';
            if (!empty($this->groupBy)){
                $jtCount = "DISTINCT ".str_replace('GROUP BY', '', $this->groupBy );
            } else {
                if (!empty($this->join)) {
                    $table = $this->fetch("DESCRIBE {$this->tableName};", false, $server, false)->Field;
                }
                $jtCount = (!empty($this->join)) ? "distinct " . str_replace(["`", " "], ["", ""], $this->tableName) . '.' . $table : $table;
            }

            $this->queryString = 'SELECT COUNT(' . $jtCount . ') as cnt FROM ' . $this->tableName . '  ' . $this->join . ' ' . $this->whereString .' ' . $this->groupBy . ';';
            $list = $this->fetch($this->queryString, false, $server, false);
            if (!isset($list->cnt)){
                $list = new \stdClass();
                $list->cnt  = 0;
            }
            $pages = ceil($list->cnt / $limit);
            $page = min($pages, filter_input(INPUT_GET, $key, FILTER_VALIDATE_INT, array(
                "options" => array(
                    "default" => 1,
                    "min_range" => 1
                )
            )));
            $offset = (($page) ? $page - 1 : 0) * $limit;
            // $start  = $offset + 1;
            $start = $offset;
            $end = min(($offset + $limit), $list->cnt);
            $this->column = (strlen($this->column) > 1) ? $this->column : ' * ';
            $this->queryString = 'SELECT ' . $this->column . ' FROM ' . $this->tableName . '  ' . $this->join . ' ' . $this->whereString .' ' . $this->groupBy . ' '. $this->queryOrder . ' LIMIT ' . $limit . ' OFFSET ' . $offset . ';';
            $this->result = $this->fetch($this->queryString, true, $server, false);
            $pList = '<nav aria-label="Page navigation" class="price_list_pagination"><ul class="pagination">';

            if ($page > 1) {
                $this->pageReqs = array_merge($this->pageReqs, [$key => 1]);
                $this->httpQuery = http_build_query($this->pageReqs);
                $pList .= '<li class="page-item"><a class="page-link" href="' . $url . '?' . $this->httpQuery . '" onClick="Med.go(this.href); return false;"><i class="fa fa-arrow-left" aria-hidden="true"></i></a></li>';
            }
            $get_add = $page + 5;
            for ($i = 1; ($i <= $pages && $pages != 1); $i++) {
                $active = ($page == $i) ? 'active' : '';
                if ($i < $get_add || $i == $get_add - 1) {
                    if ($i > $get_add - 7) {
                        $this->pageReqs = array_merge($this->pageReqs, [$key => $i]);
                        $this->httpQuery = http_build_query($this->pageReqs);
                        $pList .= '<li class="page-item ' . $active . '"><a class="page-link" href="' . $url . '?' . $this->httpQuery . '" onClick="Med.go(this.href); return false;">' . $i . '</a></li>';
                    }
                }
            }
            if ($pages > 10 && $page != $pages) {
                $this->pageReqs = array_merge($this->pageReqs, [$key => $pages]);
                $this->httpQuery = http_build_query($this->pageReqs);
                $pList .= '<li class="page-item disabled"><a class="page-link">...</a></li> <li class="page-item ' . (($pages == $page) ? 'active' : '') . '"><a class="page-link" href="' . $url . '?' . $this->httpQuery . '" onClick="Med.go(this.href); return false;">' . $pages . '</a></li>';
            }
            if ($page < $pages && $pages > 5) {
                $this->pageReqs = array_merge($this->pageReqs, [$key => $pages]);
                $this->httpQuery = http_build_query($this->pageReqs);
                $pList .= '<li class="page-item"><a class="page-link" href="' . $url . '?' . $this->httpQuery . '" onClick="Med.go(this.href); return false;"><i class="fa fa-arrow-right" aria-hidden="true"></i>
</a></li>';
            }
            $pList .= '</ul></nav>';
            $result = new \stdClass();
            $result->rows = $this->result;
            $result->pages = $pList;
            $this->close();
            return $result;
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Hey developer you will call <b>table</b> method in first!");
        }
    }

    /**
     * @param int $limit
     * @param int $section
     * @param string $key
     * @param bool $server
     * @return \stdClass
     */

    public function getAjaxPages($limit = 10, $section = 1, $key = 'pagination', $server = false)
    {
        if ($this->tableName) {
            $table = '*';
            if (!empty($this->groupBy)){
                $jtCount = "DISTINCT ".str_replace('GROUP BY', '', $this->groupBy );
            } else {
                if (!empty($this->join)) {
                    $table = $this->fetch("DESCRIBE {$this->tableName};", false, $server, false)->Field;
                }
                $jtCount = (!empty($this->join)) ? "distinct " . str_replace(["`", " "], ["", ""], $this->tableName) . '.' . $table : $table;
            }

            $this->queryString = 'SELECT COUNT(' . $jtCount . ') as cnt FROM ' . $this->tableName . '  ' . $this->join . ' ' . $this->whereString .' ' . $this->groupBy . ';';
            $list = $this->fetch($this->queryString, false, $server, false);
            if (!isset($list->cnt )){
                $list = new \stdClass();
                $list->cnt  = 0;
            }
            $pages = ceil($list->cnt / $limit);
            $page = min($pages, $section);
            $offset = (($page) ? $page - 1 : 0) * $limit;
            $start = $offset;
            $end = min(($offset + $limit), $list->cnt);
            $gaCols = (!empty($this->join)) ? str_replace('`', '', $this->tableName) . '.* ' : ' * ';
            $this->queryString = 'SELECT  ' . (empty($this->column) ? $gaCols : $this->column) . ' FROM ' . $this->tableName . ' ' . $this->join . ' ' . $this->whereString . ' ' . $this->groupBy . ' ' . $this->queryOrder . ' LIMIT ' . $limit . ' OFFSET ' . $offset . ';';
            $this->result = $this->fetch($this->queryString, true, $server, false);
            $result = new \stdClass();
            $result->rows = $this->result;
            $result->pages = new \stdClass();
            $result->pages->current = $page;
            $result->pages->pages = $pages;
            $result->pages->start = $start;
            $result->pages->end = $end;
            $result->pages->count = $list->cnt;
            $this->close();
            return $result;
        } else {
            die("<h1 style=\"color:blue\">Quasar</h1><hr></br>Hey developer you will call <b>table</b> method in first!");
        }
    }

    /**
     * @param bool $server
     * @return $this
     */
    public function clean($server = false)
    {
        $this->queryString = '';
        $this->whereString = '';
        $this->result = [];
        $this->server = 'default';
        $this->whereString = '';
        $this->order = [];
        $this->column = '';
        $this->values = '';
        $this->pageReqs = [];
        $this->httpQuery = [];
        $this->join = '';
        $this->groupBy = '';
        $this->queryOrder = '';
        return $this;
    }

    /**
     * @param bool $server
     * @return bool
     */
    public function close($server = false)
    {
        $this->queryString = '';
        $this->whereString = '';
        $this->column = '';
        if (isset($this->connection)) {
            mysqli_close($this->connection);
            $this->inited = [];
            $this->authed = false;
            $this->connection = [];
            $this->result = [];
            $this->server = 'default';
            $this->tableName = '';
            $this->whereString = '';
            $this->order = [];
            $this->column = '';
            $this->values = '';
            $this->pageReqs = [];
            $this->httpQuery = [];
            $this->join = '';
            $this->groupBy = '';
            $this->queryOrder = '';
            return true;
        } else {
            return false;
        }
    }

}
