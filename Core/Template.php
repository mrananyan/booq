<?php
/**
 * @project Galaxy Framework
 * @author Sargis Ananyan <ananyan@dr.com>
 * @version 1.0.1 alpha
 */

namespace Galaxy;

use Galaxy\Database;

class  Template{
    public $dir = __dir__. '/../Views';
    protected $template = null;
    protected $copy_template = null;
    protected $foreach_data = '';
    protected $data = array ();
    protected $arrays = array ();
    protected $vars = array ();
    protected $find = array ();
    protected $replace = array ();
    protected $find_preg = array ();
    protected $replace_preg = array ();
    protected $block_data = array ();
    public $result = array ('_nav' => '', '_header' => '', '_content' => '', '_footer' => '' , '_js' => '' );
    protected $allow_php_include = true;
    protected $html_comment = false;

    /**
     * Template constructor.
     * @param bool $allow_php
     * @param bool $htnl_comment
     */
    public function __construct($allow_php = true, $htnl_comment = false) {
        $this->allow_php_include = $allow_php;
        $this->html_comment = $htnl_comment;
    }

    /**
     * @param $name
     * @param $var
     */
    public function var($name, $var){
        $this->vars[$name] = $var;

    }

    /**
     * @param $name
     * @param $array
     */
    public function array($name, $array){
        if (is_array($array)){
            $this->arrays[$name] = $array;
        }else{
            $this->arrays[$name] = [];
        }
    }

    /**
     * @param $name
     * @param string $var
     */
    public 	function set($name, $var = '') {
        if( is_array( $name ) && count( $name ) ) {
            foreach ( $name as $key => $val) {
                $this->data[$key] = $val;
            }
        }else{
            $this->data[$name] = $var;
        }
    }

    /**
     * @param $name
     * @param string $var
     */
    public function set_block($name, $var = '') {
        if( is_array( $var ) && count( $var ) ) {
            foreach ( $var as $key => $val) {
                $this->set_block( $key, $val);
            }
        }else{
            $this->block_data[$name] = $var;
        }
    }

    /**
     * @param $name
     * @param string $var
     * @return $this
     */
    public function block($name, $var = '') {
        if (is_array($name)){
            foreach ($name as $item){
                $this->set_block("'\\[{$item}\\](.*?)\\[/{$item}\\]'si");
            }
        }else{
            $this->set_block("'\\[{$name}\\](.*?)\\[/{$name}\\]'si");
        }
        return $this;
    }

    /**
     * @param $tpl_name
     * @return bool
     */
    public function load($tpl_name) {
        $tnex = explode('.', $tpl_name);
        if (!in_array(end($tnex), ['tpl', 'html', 'js', 'css', 'php'])){
            $tpl_name = $tpl_name.'.tpl';
        }
        if( $tpl_name == '' || ! file_exists( $this->dir . DIRECTORY_SEPARATOR . $tpl_name ) ) {
            die( "<h1>Pulsar template engine</h1><hr><p><b>{$tpl_name}</b> template file not found</p>");
        }

        $this->template = file_get_contents( $this->dir . DIRECTORY_SEPARATOR . $tpl_name );

        if (strpos ($this->template, "[if " ) !== false) {
            $this->template = preg_replace_callback ( "#\\[if (.+?)\\](.*?)\\[/if\\]#is", function($m){
                return $this->if_else($m[1], $m[2]);
            }, $this->template );
        }

        if (strpos ($this->template, "[foreach " ) !== false) {
            $this->template = preg_replace_callback ( "#\\[foreach (.+?)\\](.*?)\\[/foreach\\]#is", function($m){
                return $this->foreach($m[1], $m[2]);
            }, $this->template );
        }

        if (strpos ($this->template, "{*" ) !== false) {
            $this->template = preg_replace_callback ( "#{\\*(.*?)\\*}#is", function($m){
                return $this->comment($m[0], $m[1]);
            }, $this->template );
        }


        if (strpos ( $this->template, "[not-group=") !== false) {
            $this->template = preg_replace_callback ("#\\[not-group=(.+?)\\](.*?)\\[/not-group\\]#is", function($m){
                return $this->group($m[1], $m[2], false);
            }, $this->template );
        }

        if (strpos ($this->template, "[group=") !== false) {
            $this->template = preg_replace_callback ("#\\[group=(.+?)\\](.*?)\\[/group\\]#is", function($m){
                return $this->group($m[1], $m[2]);
            }, $this->template );
        }

        if (strpos ($this->template, "[plan=") !== false) {
            $this->template = preg_replace_callback ("#\\[plan=(.+?)\\](.*?)\\[/plan\\]#is", function($m){
                return $this->plan($m[1], $m[2]);
            }, $this->template );
        }

        if (strpos ( $this->template, "[not-plan=") !== false) {
            $this->template = preg_replace_callback ("#\\[not-plan=(.+?)\\](.*?)\\[/not-plan\\]#is", function($m){
                return $this->plan($m[1], $m[2], false);
            }, $this->template );
        }

        $this->copy_template = $this->template;
        return true;
    }

    private function if_else($condition, $block) {
        global $GLOBALS;
        extract($GLOBALS, EXTR_SKIP, "");
        if(is_array($matches=explode("[else]",$block))) {
            $block=$matches[0];
            $else=$matches[1];
        }
        if(eval(("return $condition;"))) return str_replace( '\"', '"', $block );
        return str_replace( '\"', '"', $else );
    }

    /**
     * @param $condition
     * @param $item
     * @return bool|string
     */
    private function foreach($condition, $item){
        global $GLOBALS;
        extract($GLOBALS, EXTR_SKIP, "");
        $db = new Database();
        $parms = explode('as', $condition);
        $parmsKV = explode('=>', $parms['1']);
        $array = str_replace(' ', '',$parms['0']);
        if (isset($this->arrays[$array]) && is_array($this->arrays[$array])){
            if (isset($parmsKV['1'])){
                $k = str_replace(' ', '',$parmsKV['0']);
                $v = str_replace(' ', '',$parmsKV['1']);
                $this->foreach_data = '';
                foreach($this->arrays[$array] as $parmsKV['0'] => $parmsKV['1']){
                    if (is_array($parmsKV['1'])){
                        $s = [$k];
                        $r = [$parmsKV['0']];
                        foreach ($parmsKV['1'] as $key => $val){
                            array_push($s, $v."['$key']");
                            array_push($r, $val);
                            //  $this->foreach_data .= str_replace([$k, $v."['$key']"], [$parmsKV['0'], $val],$item);
                        }
                        $this->foreach_data .= str_replace($s, $r, $item);
                    }else{
                        $this->foreach_data .= str_replace([$k, $v], [$parmsKV['0'], $parmsKV['1']],$item);
                    }
                }
            }else{
                $parm = str_replace(' ', '',$parms['1']);
                $this->foreach_data = '';
                foreach($this->arrays[$array] as $parms['1']){
                    $this->foreach_data .= str_replace($parm,$parms['1'],$item);
                }
            }
            return $this->foreach_data;
        }else{
            return false;
        }
    }

    /**
     * @param $all
     * @param $inner
     * @return mixed
     */
    private function comment($all, $inner){
        if ($this->html_comment){
            return str_replace(['{*', '*}'], ['<!--', '-->'], $all);
        }else{
            return str_replace(['{*', '*}', $inner], ['', '', ''], $all);
        }
    }

    /**
     * @param $groups
     * @param $block
     * @param bool $action
     * @return mixed|string
     */
    private function group($groups, $block, $action = true) {
        global $row;
        $groups = explode( ',', $groups );
        if( $action ) {
            if(isset($row->type) AND !in_array( $row->type, $groups ) ) return "";
        } else {
            if(isset($row->type) AND in_array( $row->type, $groups ) ) return "";
        }

        $block = str_replace( '\"', '"', $block );
        return $block;
    }

    /**
     * @param $groups
     * @param $block
     * @param bool $action
     * @return mixed|string
     */
    private function plan($groups, $block, $action = true) {
        global $row;

        /** @var Database $db */
        $db = new Database();

        $groups = explode( ',', $groups );
        $plan = $db->table('billing')->select('plan')->where([['uid', '=', $row->id]])->order('id', 'DESC')->get();

        $plan = (empty($plan)) ? $row->plan : $plan->plan;

        if( $action ) {
            if(isset($plan) AND !in_array( $plan, $groups ) ) return "";
        } else {
            if(isset($plan) AND in_array( $plan, $groups ) ) return "";
        }

        $block = str_replace( '\"', '"', $block );
        return $block;
    }

    /**
     * @param $var
     * @return string
     */
    private function translate($var){
        global $lang;
        return (isset($lang[$var])) ? $lang[$var] : '';
    }

    /**
     * @param $all
     * @param $tpl_name
     * @return false|string
     */
    private function include($all, $tpl_name){
        if( $tpl_name == '' || ! file_exists( $this->dir . DIRECTORY_SEPARATOR . $tpl_name ) ) {
            die( "<h1>Pulsar template engine</h1><hr><p><b>{$tpl_name}</b> template file not found</p>");
        }

        $included = file_get_contents( $this->dir . DIRECTORY_SEPARATOR . $tpl_name );

        return $included;
    }

    /**
     *
     */
    private function clear() {
        $this->data = array ();
        $this->block_data = array ();
        $this->find = array ();
        $this->replace = array ();
        $this->copy_template = $this->template;
    }

    /**
     * @param string $tpl
     */
    public function make($tpl = '_content') {
        $this->copy_template = preg_replace_callback("#\\{include=(.+?)\\}#is", function($m){
            return $this->include($m[0], $m[1]);
        }, $this->copy_template);

        global $logged, $nav, $row;
        if( count( $this->block_data ) ) {
            foreach ( $this->block_data as $key_find => $key_replace ) {
                $this->find_preg[] = $key_find;
                $this->replace_preg[] = $key_replace;
            }

            $this->copy_template = preg_replace( $this->find_preg, $this->replace_preg, $this->copy_template );
        }

        foreach ( $this->data as $key_find => $key_replace ) {
            $this->find[] = $key_find;
            $this->replace[] = $key_replace;
        }

        $this->copy_template = str_replace( $this->find, $this->replace, $this->copy_template );

        if($logged){
            $this->copy_template = preg_replace_callback("'\\[not-logged\\](.*?)\\[/not-logged\\]'si", function($m){
                return $this->set_block($m[1]);
            }, $this->copy_template);
            $this->copy_template = str_replace(array("[logged]", "[/logged]"), array("", ""), $this->copy_template);
        } else {
            $this->copy_template = preg_replace_callback("'\\[logged\\](.*?)\\[/logged\\]'si", function($m){
                return $this->set_block($m[1]);
            }, $this->copy_template);
            $this->copy_template = str_replace(array("[not-logged]", "[/not-logged]"), array("", ""), $this->copy_template);
        }

        if($nav){
            $this->copy_template = preg_replace_callback("'\\[not-nav\\](.*?)\\[/not-nav\\]'si", function($m){
                return $this->set_block($m[1]);
            }, $this->copy_template);
            $this->copy_template = str_replace(array("[nav]", "[/nav]"), array("", ""), $this->copy_template);
        } else {
            $this->copy_template = preg_replace_callback("'\\[nav\\](.*?)\\[/nav\\]'si", function($m){
                return $this->set_block($m[1]);
            }, $this->copy_template);
            $this->copy_template = str_replace(array("[not-nav]", "[/not-nav]"), array("", ""), $this->copy_template);
        }

        if(isset($row->online) AND $row->online == 0){
            $this->copy_template = preg_replace_callback("'\\[not-first\\](.*?)\\[/not-first\\]'si", function($m){
                return $this->set_block($m[1]);
            }, $this->copy_template);
            $this->copy_template = str_replace(array("[first]", "[/first]"), array("", ""), $this->copy_template);
        } else {
            $this->copy_template = preg_replace_callback("'\\[first\\](.*?)\\[/first\\]'si", function($m){
                return $this->set_block($m[1]);
            }, $this->copy_template);
            $this->copy_template = str_replace(array("[not-first]", "[/not-first]"), array("", ""), $this->copy_template);
        }

        $this->copy_template = preg_replace_callback("#\\{translate=(.+?)\\}#is", function($m){
            return $this->translate($m[1]);
        }, $this->copy_template);


        if( isset( $this->result[$tpl] ) ){
            $this->result[$tpl] .= $this->copy_template;
        }else{
            $this->result[$tpl] = $this->copy_template;
        }
        $this->clear();
    }

    /**
     * @param $name
     * @return mixed|string
     */
    public function get($name){
        global $GLOBALS;
        extract($GLOBALS, EXTR_SKIP, "");
        ob_start();
        eval (' ?' . '>' . $this->result[$name] . '<' . '?php ');
        $content =  ob_get_contents();
        ob_end_clean();
        return (isset($this->result[$name])) ? $content : '';
    }

    /**
     * @param $buffer
     * @return string|string[]|null
     */
    public function minify($buffer){
        $search  = array(
            '/\>[^\S ]+/s',
            '/[^\S ]+\</s',
            '/(\s)+/s',
            '/<!--(.|\s)*?-->/'
        );
        $replace = array(
            '>',
            '<',
            '\\1',
            ''
        );
        $buffer  = preg_replace($search, $replace, $buffer);
        return $buffer;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function display($name = '_content'){
        if ($this->allow_php_include){
            if (isset($this->result[$name])){
                eval (' ?' . '>' . $this->result[$name] . '<' . '?php ');
            }else{
                return false;
            }
        }else{
            echo (isset($this->result[$name])) ? $this->result[$name] : '';
        }
        die();
    }
}