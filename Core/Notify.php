<?php
/**
 * @project Galaxy Framework
 * @author Sargis Ananyan <ananyan@dr.com>
 * @version 1.0.1 alpha
 */

namespace Galaxy;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Notify implements MessageComponentInterface {
    protected $clients;
    public $users = [];

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        $querystring = $conn->httpRequest->getUri()->getQuery();
        parse_str($querystring,$queryarray);
        $this->clients->attach($conn);
        if (isset($queryarray['token'])){
            $this->users['uid'.$queryarray['token']] = $conn;
        }

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $msg = json_decode($msg, true);
        if (isset($msg['to']) && isset($this->users[$msg['to']])){
            $this->users[$msg['to']]->send($msg['message']);
        }

    }

    public function onClose(ConnectionInterface $conn) {
        $querystring = $conn->httpRequest->getUri()->getQuery();
        parse_str($querystring,$queryarray);
        $this->clients->detach($conn);
        if (isset($queryarray['token'])){
            unset( $this->users['uid'.$queryarray['token']]);
        }
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";
        $querystring = $conn->httpRequest->getUri()->getQuery();
        parse_str($querystring,$queryarray);
        if (isset($queryarray['token'])){
            unset( $this->users['uid'.$queryarray['token']]);
        }
        $conn->close();
    }
}