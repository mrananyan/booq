<?php
/**
 * @project Galaxy Framework
 * @author Sargis Ananyan <ananyan@dr.com>
 * @version 1.0.1 alpha
 */

namespace Galaxy;

use Symfony\Component\HttpFoundation\Request;
use Galaxy\Template;

class Controllers extends Template
{
    protected $path = Controllers;
    public $class;
    public $url;
    public $method;

    public function __construct(Request $request)
    {
        $query_array = explode('/', $request->getPathInfo());
        if (empty($query_array[1])){
            $query_array[1] = 'index';
        }
        if (!isset($query_array[2]) OR empty($query_array[2])){
            $query_array[2] = 'index';
        }
        foreach ($query_array as $i => $qa){
            if (!in_array($i, [0,1,2])){
                $_GET['segment_'.$i] = $qa;
            }
        }
        $this->method = ucfirst(preg_replace("/[^A-Za-z0-9?!]/", '', $query_array[2]));
        $this->class = ucfirst(preg_replace("/[^A-Za-z0-9?!]/", '', $query_array[1])).'Controller';
        $this->url = ucfirst(preg_replace("/[^A-Za-z0-9?!]/", '', $query_array[1]));
        return $this;
    }

    public function sf(... $files){
        global $st_files;
        foreach ($files as $file){
            $extension = explode('.', $file);
            if (in_array(end($extension), ['js', 'css', 'ljs'])){
               if (!isset($st_files[$file])){
                   array_push($st_files, $file);
               }
            }
        }
    }

    public function json($array){
        header('Content-Type: application/json');
        echo json_encode($array);
        exit;
    }

    public static function content($name, $vars = []){
        global $GLOBALS, $tpl;
        $tpl->load($name);
        if (count($vars) > 0){
            $tpl->set($vars);
        }
        $tpl->make('content');
    }

    public static function js($name, $vars = []){
        global $GLOBALS, $tpl;
        $tpl->load($name);
        if (count($vars) > 0){
            $tpl->set($vars);
        }
        $tpl->make('js');
    }

    public static function header($name, $vars = []){
        global $GLOBALS, $tpl;
        $tpl->load($name);
        if (count($vars) > 0){
            $tpl->set($vars);
        }
        $tpl->make('header');
    }

    public static function sidebar($name, $vars = []){
        global $GLOBALS, $tpl;
        $tpl->load($name);
        if (count($vars) > 0){
            $tpl->set($vars);
        }
        $tpl->make('sidebar');
    }

    public static function footer($name, $vars = []){
        global $GLOBALS, $tpl;
        $tpl->load($name);
        if (count($vars) > 0){
            $tpl->set($vars);
        }
        $tpl->make('footer');
    }

    public function lang($name){
        global $initializeLanguages;
        $initializeLanguages->load($name);
    }

    public function date($timestamp = false, $format = "D, d M Y H:i"){
        global $lang;
        if (!$timestamp){
            $timestamp = time();
        }
        return strtr(@date($format, $timestamp), $lang['lang_date']);
    }
}