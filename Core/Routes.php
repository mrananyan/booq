<?php
/**
 * @project Galaxy Framework
 * @author Sargis Ananyan <ananyan@dr.com>
 * @version 1.0.1 alpha
 */

namespace Galaxy;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Galaxy\Controllers;

class Routes
{
    private $controller;
    public $new;

    public function __construct()
    {
        global $cooLang;
        $request = Request::createFromGlobals();
        $response = new Response(
            'Content',
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );
        @$this->controller = new \Galaxy\Controllers($request);
        if (class_exists('\Controllers\\' . $this->controller->class)) {
            $this->new = true;
            /*if (file_exists(App . '/langs/' . $cooLang . '/'.$this->controller->url.'.php')){
                include App . '/langs/' . $cooLang . '/'.$this->controller->url.'.php';
            }*/
            $this->controller->class = '\Controllers\\' . $this->controller->class;
            $Controller = new $this->controller->class($request);
            if (!empty($this->controller->method) OR $this->controller->method !== null) {
                if (method_exists($Controller, $this->controller->method)) {
                    $ar = new \ReflectionMethod($this->controller->class, $this->controller->method);
                    $args = $ar->getParameters();
                    if (count($args) == 1) {
                        $Controller->{$this->controller->method}($request);
                    } else if (count($args) == 2) {
                        $Controller->{$this->controller->method}($request, $response);
                    } else {
                        $Controller->{$this->controller->method}();
                    }
                } else {
                    header('Location: /error404');
                }
            } else {
                if (method_exists($Controller, 'index')) {
                    $this->controller->method = 'index';
                    $ar = new \ReflectionMethod($this->controller->class, $this->controller->method);
                    $args = $ar->getParameters();
                    if (count($args) > 0) {
                        $Controller->{$this->controller->method}($request);
                    } else {
                        $Controller->{$this->controller->method}();
                    }
                } else {
                    header('Location: /error404');
                }
            }
        } else {
            global $tpl, $initializeLanguages, $title, $lang;
            http_response_code(404);
            $initializeLanguages->load('errors/404');
            $title = $lang['404_title'];
            $tpl->load('errors/404.php');
            $tpl->make('content');
            $this->new = false;
        }
    }
}
