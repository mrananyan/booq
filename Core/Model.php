<?php
/**
 * @project Galaxy Framework
 * @author Sargis Ananyan <ananyan@dr.com>
 * @version 1.0.1 alpha
 */

namespace Galaxy;

use Galaxy\Database;

class Model extends Database
{
    public $db;
    public $table;

    /**
     * Services constructor.
     */
    public function __construct()
    {
        $this->db = $this->table($this->table);
        if (method_exists($this,'default')){
            $this->default();
        }
    }

    /**
     * @return array|bool|object|\stdClass|null
     */
    public function all()
    {
        return $this->db->table($this->table)->clean()->getAll();
    }

    /**
     * @return array|bool|object|\stdClass|null
     */
    public function first()
    {
        return $this->db->table($this->table)->order('id', 'DESC')->get();
    }

    /**
     * @return array|bool|object|\stdClass|null
     */
    public function last()
    {
        return $this->db->table($this->table)->order('id', 'ASC')->get();
    }

    /**
     * @param array $where
     * @return bool
     */
    public function destroy($where = array())
    {
        if (count($where) >= 1) {
            $this->db->clean()->table($this->table)->where($where)->delete();
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param array $data
     * @return array|bool|\mysqli_result
     */
    public function store($data = array())
    {
        if (count($data) >= 1) {
            $lastId = $this->db->clean()->table($this->table)->insert($data);
            return $lastId;
        } else {
            return true;
        }
    }

    /**
     * @param array $where
     * @param array $data
     * @return bool
     */
    public function upgrade($where = array(), $data = array())
    {
        if (is_array($where) && count($where) >= 1 AND count($data) >= 1) {
            $this->db->clean()->table($this->table)->where($where)->update($data);
            return true;
        } else {
            return false;
        }
    }
}