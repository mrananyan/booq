<?php
/**
 * @project Galaxy Framework
 * @author Sargis Ananyan <ananyan@dr.com>
 * @version 1.0.1 alpha
 */

namespace Galaxy;

class Languages{
    public $language_iso;
    public function __construct(){
        global $lang;
        $memcache_enabled = false;
        if (class_exists('Memcached')){
            $memcached = new \Memcached();
            $memcached->addServer(getenv('MEM_HOST'), getenv('MEM_PORT'));
            $memcache_enabled = true;
        }

        if (isset($_COOKIE['language'])){
           $this->language_iso =  $_COOKIE['language'];
        } elseif (isset($_SESSION['language'])){
            $this->language_iso =  $_COOKIE['language'];
        }elseif ($memcache_enabled AND $memcached->get('language')){
            $this->language_iso = $memcached->get('language');
        }else{
            $this->language_iso = 'en';
        }
        @setlocale(LC_ALL, $this->language_iso);
    }

    public function core(){
        global $lang;
        require_once dirname(__DIR__).'/Languages/'.$this->language_iso.'/core.php';
        $lang['lang_date'] = $language['lang_date'];
    }

    public function load($name){
        global $lang;
        require_once dirname(__DIR__).'/Languages/'.$this->language_iso.'/'.$name.'.php';
        foreach ($language as $key => $value){
            $lang[$key] = $value;
        }
    }
}