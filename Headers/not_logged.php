<?php
/**
 * @project booq CRM
 * @author Sargis Ananyan <ananyan@dr.com>
 * @version 1.0.0 dev``
 */

global $tpl, $initializeLanguages;
$initializeLanguages->load('headers');

$tpl->load('headers/not_logged.php');
$tpl->make('header');