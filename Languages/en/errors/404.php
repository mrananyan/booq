<?php
$language['404_title'] = 'Error 404 - not found';
$language['404_desc'] = 'Web page or file you are required not found or deleted. Please re-check the url and try again or return back to homepage.';