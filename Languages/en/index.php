<?php
$language['index_0'] = "Sign-in to booq";
$language['index_1'] = "E-mail";
$language['index_2'] = "Password";
$language['index_3'] = "About booq";
$language['index_4'] = "remember me";
$language['index_5'] = "Your session will not be closed automatically even you will close browser or restart your computer";
$language['index_6'] = "Your session will be closed automatically after closing browser tab";