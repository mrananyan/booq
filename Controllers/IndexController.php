<?php

namespace Controllers;

use Galaxy\Controllers;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controllers
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    public function index(Request $request)
    {
        $this->lang('index');
        $this->sf('buttons.js', 'buttons.css', 'login.css', 'checkboxes.js', 'checkboxes.css', 'boxes.js', 'boxes.css', 'index.js');
        $this->content('index/index.php');
        $this->js('index/index.js');
    }

}