function login(form) {
    const data = serialize(form);
    ajax.post('/auth/login', data, function (response) {
        if (response.error){
            showMessage(response.message, MESSAGE_ERROR, {left: true});
        }
    });
    console.log();
    return false;
}