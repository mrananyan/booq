<?php
/**
 * @project Galaxy Framework
 * @author Sargis Ananyan <ananyan@dr.com>
 * @version 1.0.1 alpha
 */

session_start();
include dirname(__DIR__).'/vendor/autoload.php';
include dirname(__DIR__).'/initialize.php';