<?php
/**
 * @project Galaxy Framework
 * @author Sargis Ananyan <ananyan@dr.com>
 * @version 1.0.1 alpha
 */

use Galaxy\Template;
use Galaxy\Routes;
use Galaxy\Languages;

//initialize environment
$env = Dotenv\Dotenv::createImmutable(__DIR__);
$env->load();
// Translations list for i18n
$lang = [];
// initialize i18n
$initializeLanguages = new Languages();
$initializeLanguages->core();
// Static files list
$st_files = [];
$st_res = PHP_EOL;
//Default title
$title = false;

// Check if user is logged
$logged = false;

// Initialize template engine
$tpl = new Template();
// Load the routes
$router = new Routes();

// Load headers
if ($logged){
    require_once __DIR__."/Headers/logged.php";
}else{
    require_once __DIR__."/Headers/not_logged.php";
}

// Load the main frame
$tpl->load('main.php');
foreach ($st_files as $file) {
    if (strpos($file, '.js') !== false) {
        $st_res .= '<script type="text/javascript" src="/js/' . $file . '"></script>';
    } else {
        $st_res .= '<link rel="stylesheet" type="text/css" href="/css/' . $file . '"/>';
    }

}
$timestamp = time();
$tpl->set('{st_files}', $st_res);
$tpl->set([
    '{language_iso}' => $initializeLanguages->language_iso,
    '{title}' => $title ?: getenv('TITLE'),
    '{tab_id}' => base64_encode($timestamp*$timestamp),
    '{header}' => $tpl->get('header'),
    '{content}' => $tpl->get('content'),
    '{sidebar}' => $tpl->get('sidebar'),
    '{footer}' => $tpl->get('footer'),
    '{js}' => $tpl->get('js'),
]);
// Make the main frame
$tpl->make('galaxy');
// Compile and print the main frame
$tpl->display('galaxy');
